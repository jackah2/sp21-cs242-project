# A web app called RTHM
Jack Henhapl (jackah2) | Moderator: Brian Huang (brianbh2)

This is a mobile app about a project for CS242

## Abstract
### Project Purpose
The purpose of this project is to provide a platform to write percussive music in real-time by playing it through your device.

### Project Motivation
Writing music for drumline is cumbersome and difficult in existing music notation software.
I want to create an application that allows the user to play a rhythm using their keyboard and have it automatically converted to music.

## Technical Specification
- [MusicXML](https://en.wikipedia.org/wiki/MusicXML) export format
- MongoDB
- Platform: Web app (React)
- Programming Languages: JavaScript (Python for Flask backend)
- Stylistic Conventions: Airbnb JavaScript Style Guide
- IDE: Visual Studio Code
- Tools/Interfaces: Web browsers
- Target Audience: Percussion musicians

## Functional Specification
### Features
- An adjustable metronome
- The rhythm can be exported to a file (to later be imported into other music notating software)
- Rhythm quantization
- Support for stickings (right/left)
- A complete API that allows a frontend to create, modify, and delete rhythms
  - This is useful for other applications that would like to hook into the system. I currently just plan on using it for communication with the frontend as I would prefer to write the backend in Python.

### Scope of the project
- I will not be messing around with pitch. This is mainly about rhythm

## Brief Timeline
- Week 1: Create function to convert timed notes to music
- Week 2: Have a sense of "users" and allow them to save their rhythms to a server
- Week 3: Create Flask API. I plan on having the API follow the CRUD pattern.
- Week 4: Create the frontend

## Rubrics
### [Week 1](https://docs.google.com/spreadsheets/d/1z1KnDxVK9JllWUHv7HnARNQL9Is9r0j7zoL8bYUd97I/edit?usp=sharing)
| Category  | Total Score Allocated | Detailed Rubrics |
|-|:-:|-|
| Rhythm Quantization |  4  |  0: Didn't implement anything <br> 1: Did not implement quantization <br> 3: Implemented quantization for some rhythms <br> 4: completed function |
| Identify flams |  2  |  0: Didn't implement anything <br> 2: completed function |
| Convert groups of notes into tuplets |  4  |  0: Didn't implement anything <br> 1: Implemented even numbered tuplets <br> 3: Implemented triplets <br> 4: completed function (up to sextuplets) |
| Sequenced notes to XML |  5  |  0: Didn't implement anything <br> 1: Support basic notes (quarter, eighth notes) <br> 3: Support tuplets <br> 5: completed function (including flams) |
|  Test exporting to XML |  6  |  0: Didn't implement tests <br> 1: Implemented basic notes <br> 3: Implemented tests with flams <br> 6: completed test |
|  Test quantization |  4  |  0: Didn't implement tests <br> 1: implemented tests for basic quantization <br> 3: implemented tests with advanced tuplets <br> 4: completed test |

### [Week 2](https://docs.google.com/spreadsheets/d/1Uvvy0s6Al21iZi8Qj_6Z_Ic-Bv9CCXLGZPAGfJAUqgA/edit?usp=sharing)
| Category  | Total Score Allocated | Detailed Rubrics |
|-|:-:|-|
|  Save rhythms to a database |  4  |  0: Didn't implement anything <br> 1: Presence of queries <br> 3: Connecting and inserting into database <br> 4: completed function |
|  Support sticking in rhythms |  2  |  0: Didn't implement anything <br> 2: completed function |
|  Search for rhythms |  4  |  0: Didn't implement anything <br> 1: Find all rhythms <br> 3: Refine search to be by user <br> 4: Rhythms can be searched by their actual structure |
|  Allow attributes of saved rhythms to be changeable |  5  |  0: Didn't implement anything <br> 1: Change name <br> 3: Rhythm is deletable <br> 5: You can change the rhythm itself |
|  Test database actions |  6  |  0: Didn't implement tests <br> +1 point per 2 unit tests |
|  Test search |  4  |  0: Didn't implement tests <br> +1 point per 2 unit tests |


### [Week 3](https://docs.google.com/spreadsheets/d/1cGorjeoTaL2GDxy-ib6h8SRWHuxFHRIFL6uGjfi3-XI/edit?usp=sharing)
| Category  | Total Score Allocated | Detailed Rubrics |
|-|:-:|-|
|  GET rhythm |  4  |  0: Didn't implement anything <br> 1: Can get all rhythms <br> 3: No error checking <br> 4: completed function |
|  DELETE rhythm |  2  |  0: Didn't implement anything <br> 1: No error checking <br> 2: completed function |
|  POST rhythm |  4  |  0: Didn't implement anything <br> 3: No error checking <br> 4: completed function |
|  PUT new rhythm |  5  |  0: Didn't implement anything <br> 1: Basic response set up <br> 3: No error checking <br> 5: Fully implemented endpoint |
|  Tests |  10  |  +1 point per 2 tests |


### [Week 4](https://docs.google.com/spreadsheets/d/15vmOecRVdCKidExfnNzLkXGWv-p9B0o_4D4Gv3mdsFQ/edit?usp=sharing)
| Category  | Total Score Allocated | Detailed Rubrics |
|-|:-:|-|
|  Multiple routes  |  4  |  0: Didn't implement anything <br> 1: Main creation page <br> 3: Search page <br> 4: All pages (creation, search, modification pages) |
|  Adjustable metronome |  2  |  0: Didn't implement anything <br> Metronome is not adjustable <br> 2: Metronome present |
|  Allow user to change aspects of a rhythm |  4  |  0: Didn't implement anything <br> -1 point per missed attribute |
|  Can "record" rhythm input |  5  |  0: Didn't implement anything <br> 1: Does not allow for sticking <br> 3: Cannot use keyboard for input <br> 5: completed function d |
|  Manual test plan |  10  |  0: Didn't implement tests <br> 4: Comprehensive manual test plan |
