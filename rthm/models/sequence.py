"""Holds the Sequence class"""

from typing import List
import numpy as np

from rthm.models.note import Note
from rthm.models.tempo import Tempo


class Sequence:
    """
    Represents a sequence of notes. Has quantization built-in. Goes up to 16th notes and
    8th note triplets.
    """

    def __init__(self, notes: List[Note], tempo: int):
        self.tempo = Tempo(tempo)
        # Sort notes by when they were played
        self.notes = sorted(notes, key=lambda note: note.raw_time)

        spb = self.tempo.seconds_per_beat

        # Find all timings of downbeats. Extends past the last note to ensure all notes
        # can be contained within this range
        self.downbeat_timings = np.arange(
            start=0,
            stop=(self.notes[-1].raw_time // spb + 2) * spb,
            step=spb
        )

        # Create timings for tuple and triplet based subdivisions.
        self._tuple_timings = self.get_quantized_subdivisions(4)
        self._triplet_timings = self.get_quantized_subdivisions(3)

        # Find all timings of quantized subdivisions. Includes tuple and triplet timings
        self.subdivision_timings = np.concatenate((self._tuple_timings, self._triplet_timings))

        # Quantize each note then contextualize it with the next one (or None if if its the last)
        # Reverse order so the 'next' note is already quantized
        for ind, note in reversed(list(enumerate(self.notes))):
            next = self.notes[ind + 1] if ind + 1 < len(self.notes) else None
            note.quantize(self)
            note.contextualize(next)

    def get_quantized_subdivisions(self, notes_per_beat):
        return np.linspace(
            start=0,
            stop=self.downbeat_timings[-1],
            num=(len(self.downbeat_timings) - 1) * notes_per_beat + 1
        )
