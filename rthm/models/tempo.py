"""Holds the Tempo class"""

class Tempo:
    """Simple class to represent a tempo"""

    def __init__(self, bpm):
        """Create a Tempo with the given beats per minute"""
        self.bpm = bpm

    @property
    def seconds_per_beat(self):
        return 60 / self.bpm
