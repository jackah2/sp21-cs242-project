"""Holds the Rhythm class"""

from typing import List, Optional
from bson.objectid import ObjectId
from scamp import Session, StaffText

import rthm.utils.mongo as mongo
from rthm.models.sequence import Sequence
from rthm.models.note import Note


class Rhythm:
    """
    Represents a recorded rhythm. Includes author attributes and metadata regarding the
    creation time.
    """

    @staticmethod
    def get_by_id(mongo_id: ObjectId) -> Optional["Rhythm"]:
        """Retrieve a Rhythm by its Mongo ID"""
        doc = mongo.COLLECTION.find_one({'_id': mongo_id})
        return Rhythm(**doc) if doc else None

    @staticmethod
    def get_by_id_str(mongo_id: str) -> Optional["Rhythm"]:
        """Wrapper of get_by_id to allow string input"""
        return Rhythm.get_by_id(ObjectId(mongo_id))

    @staticmethod
    def get_all() -> list:
        """Get all Rhythms saved in the db"""
        return list(mongo.COLLECTION.find({}))

    @staticmethod
    def search(query: Optional[str]) -> list:
        """
        Searchs for the query within text fields within each mongo document. Returns all rhythms
        if the query is None.
        """
        if not query:
            return Rhythm.get_all()
        return list(mongo.COLLECTION.find({'$text': {'$search': query}}))

    @staticmethod
    def delete_by_id(mongo_id: ObjectId):
        """Delete a Rhythm by its Mongo ID"""
        return mongo.COLLECTION.delete_one({'_id': mongo_id})

    @staticmethod
    def delete_by_id_str(mongo_id: str):
        """Wrapper of delete_by_id to allow string input"""
        return Rhythm.delete_by_id(ObjectId(mongo_id))

    def __init__(self, title: str, author: str, timings: List[int], stickings: List[str],
            tempo: int, time_created: int, _id: ObjectId = None):
        """Initialize a Rhythm. 'timings' and 'stickings' must be the same length.

        Args:
            title (str): The title of the piece.
            author (str): The author of the piece.
            timings (list[int]): Timings of the notes played.
            stickings (list[str]): Stickings of the notes played.
            tempo (int): The tempo of the piece.
            time_created (int): The unix time the piece was created.
            _id (ObjectId): The ID of the rhythm. Defaults to a new object id
        """
        self.title = title
        self.author = author
        self.timings = timings
        self.stickings = stickings
        self.tempo = tempo
        self.time_created = time_created
        self._id = _id or ObjectId()

        self.sequence = Sequence(
            [Note(time, sticking) for time, sticking in zip(self.timings, self.stickings)],
            tempo
        )

    def get_xml_representation(self):
        """Get the MusicXML representation of this Rhythm.

        Returns:
            str: Music XML of the score.
        """
        session = Session(tempo=self.tempo)
        drum = session.new_part('MDL Snare')

        # Simple wrapper function to play a note with sticking
        def play_note(duration, sticking):
            drum.play_note(65, 1, duration, StaffText(sticking, bold=True, placement='below'))

        # Skip the actual playback of the performance
        session.fast_forward_in_beats(len(self.sequence.downbeat_timings))

        # Play back each note in the sequence to be transcribed
        session.start_transcribing()
        for note in self.sequence.notes:
            duration = note.raw_time_to_next / self.sequence.tempo.seconds_per_beat
            play_note(duration, note.sticking)
        performance = session.stop_transcribing()

        # Generate the score using scamp's built in quantization
        score = performance.to_score(
            title=self.title,
            composer=self.author,
            simplicity_preference=4
        )
        return score.to_music_xml().to_xml()

    def encode(self):
        """Get a dictionary representation of the Rhythm"""
        return {
            'title': self.title,
            'author': self.author,
            'timings': self.timings,
            'stickings': self.stickings,
            'tempo': self.tempo,
            'time_created': self.time_created,
        }

    def save_to_mongodb(self):
        """Save the Rhythm to Mongo"""
        mongo.COLLECTION.update_one(
            {'_id': self._id},
            {'$set': self.encode()},
            upsert=True
        )
