"""Holds the Note class"""

import numpy as np

from typing import Optional, TYPE_CHECKING
if TYPE_CHECKING:
    from rthm.models.sequence import Sequence

class Note:
    """
    Represents a note played. Contains sticking. Requires a Sequence to be fully quantized
    and contextualized.
    """

    def __init__(self, time: float, sticking: str):
        self.raw_time = time
        self.sticking = sticking

        # These will only be update when quantized
        self._quantized = False
        self.beat = None
        self.quantized_time = None

        # These will only be updated when contextualized
        self._contextualized = False
        self.grace_note = None
        self.raw_time_to_next = None
        self.quantized_time_to_next = None

    @property
    def quantized(self):
        return self._quantized

    def quantize(self, sequence: 'Sequence'):
        """Quantize the note assuming it falls within the given sequence"""
        self._quantized = True

        # Pick the subdivision that the note's raw time most closely matches.
        ind = np.argmin(np.abs(sequence.subdivision_timings - self.raw_time))
        self.quantized_time = sequence.subdivision_timings[ind]

        # Given the quantized time, find which beat it belongs to
        self.beat = np.searchsorted(
            sequence.downbeat_timings,
            self.quantized_time,
            side='right'
        )

    @property
    def contextualized(self):
        return self._contextualized

    def contextualize(self, next: Optional['Note']):
        """Adds additional information to the note given the next note in the sequence.

        Args:
            next (Optional[Note]): The next note that will be played or None if it's the last.
        """
        self._contextualized = True
        if next:
            # The note is a grace note if it landed _just_ before another note. This can be
            # detected if it shared the same quantized time.
            self.grace_note = next.quantized_time == self.quantized_time
            self.raw_time_to_next = next.raw_time - self.raw_time
            self.quantized_time_to_next = next.quantized_time - self.quantized_time
        else:
            self.raw_time_to_next = 1
            self.quantized_time_to_next = 1

    def __str__(self):
        gn = 'x' if self.grace_note else ''
        return f'[{self.beat}] {gn} ({self.quantized_time:.3f}) ({self.raw_time:.3f}) ({self.sticking})'
