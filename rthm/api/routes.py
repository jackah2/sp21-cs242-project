"""Holds all routes for the API"""

from bson.objectid import ObjectId
from bson.errors import InvalidId
from rthm.models.rhythm import Rhythm
from flask import Blueprint, Response, request
import time
import json
import numbers

from rthm.utils.jsonencoder import JSONEncoder


rthm = Blueprint('rthm', __name__)

@rthm.route('/<id>', methods=['GET', 'DELETE', 'POST'])
@rthm.route('/xml/<id>', methods=['GET'], endpoint='getxml')
def get_rhythm(id):
    # Validate that the given id is a valid ObjectId
    try:
        object_id = ObjectId(id)
    except InvalidId:
        return {'message': 'Invalid id'}, 400

    # Retrieve the Rhythm and return an error if it can't be found
    rhythm = Rhythm.get_by_id(object_id)
    if rhythm is None:
        return {'message': 'Rhythm not found!'}, 404

    if request.method == 'GET':
        if request.endpoint == 'rthm.getxml':
            return Response(rhythm.get_xml_representation(), mimetype='text/xml')

        # Include the object ID here to simplicity
        doc = rhythm.encode()
        doc['_id'] = str(rhythm._id)
        return doc, 200

    if request.method == 'DELETE':
        Rhythm.delete_by_id(rhythm._id)
        return {'message': f"Removed {id}"}, 200

    # At this point, we know it is a POST request
    if not request.is_json:
        return {'message': 'JSON not found'}, 415

    document = request.get_json()

    # If an attribute that doesn't exist tries to be set, return an error
    for attr, value in document.items():
        if not hasattr(rhythm, attr):
            return {'message': f"Unknown attribute '{attr}'"}, 400
        setattr(rhythm, attr, value)

    # Save the rhythm
    rhythm.save_to_mongodb()
    return {'message': f"Updated {str(rhythm._id)}"}, 200

@rthm.route('/xml', methods=['GET'], endpoint='xml')
@rthm.route('/publish', methods=['PUT'], endpoint='publish')
def create_rhythm():
    if not request.is_json:
        return {'message': 'JSON not found'}, 415

    document = request.get_json()

    # Ensure a proper tempo has been given
    tempo = document.get('tempo')
    if tempo is None:
        return {'message': "Missing 'tempo'"}, 400

    try:
        tempo = int(tempo)
    except ValueError:
        return {'message': "Invalid 'tempo'"}, 400

    if tempo <= 0:
        return {'message': "Invalid 'tempo'"}, 400

    title = document.get('title', '')
    author = document.get('author', '')

    # Ensure timings are valid
    timings = document.get('timings')
    if timings is None:
        return {'message': "Missing 'timings'"}, 400

    if not all(isinstance(timing, numbers.Number) for timing in timings):
        return {'message': 'All timings must be numbers'}, 400

    # If no stickings are given, make them all blank
    stickings = document.get('stickings')
    if stickings is None:
        stickings = [''] * len(timings)

    # Ensure timings and stickings are the same length
    if len(timings) != len(stickings):
        return {'message': "'timings' and 'sticking' vary in length"}, 400

    try:
        rhythm = Rhythm(title, author, timings, stickings, tempo, int(time.time()))
    except Exception as exc:
        return {'message': f"error: {str(exc)}"}, 500

    # Only return the XML representation if not publishing
    print(request.endpoint)
    if request.endpoint == 'rthm.xml':
        return Response(rhythm.get_xml_representation(), mimetype='text/xml')

    # Save the rhythm and return its ID
    rhythm.save_to_mongodb()
    return {'_id': str(rhythm._id)}, 200


@rthm.route('/search', methods=['GET'])
def search_rhythm():
    results = Rhythm.search(request.args.get('query'))
    return {'results': json.loads(JSONEncoder().encode(results))}, 200
