"""Entry point to start Flask API"""

from argparse import ArgumentParser

from rthm.api import create_app


parser = ArgumentParser(prog="rthm.api")
parser.add_argument(
    "--port",
    type=str,
    default=5000,
    help='The port to run the Flask app on'
)
parser.add_argument(
    "--debug",
    action='store_true',
    default=False,
    help='The port to run the Flask app on'
)

args = vars(parser.parse_args())

app = create_app()
app.run(port=args.get('port'), debug=args.get('debug'))
