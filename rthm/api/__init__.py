from flask import Flask

from rthm.api.routes import rthm


def create_app() -> Flask:
    """Get an instance of the Flask app. All routes begin with /rthm"""
    app = Flask(__name__)
    app.register_blueprint(rthm, url_prefix='/rthm')
    return app
