"""Allows the user to use keyboard input to create a Rhythm"""

import keyboard
from argparse import ArgumentParser

from rthm.models.rhythm import Rhythm


def on_press(event):
    """Listens and tracks keyboard events"""
    # The event name corresponds to the key pressed
    print(event.name)
    if event.name == 'q':
        return
    try:
        # Add the first letter of the key pressed
        on_press.presses.append((event.time - on_press.start, event.name[0]))
    except AttributeError:
        on_press.presses = [(0, event.name[0])]
        on_press.start = event.time

if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument(
        'bpm',
        type=int,
        help='Beats per minute you will play'
    )
    bpm = vars(parser.parse_args()).get('bpm')

    print('Listening to keyboard input. Press "q" to stop')
    keyboard.on_press(callback=on_press)
    while True:
        if keyboard.is_pressed('q'):
            print('Stopping')
            break

    # Stop listening after the
    keyboard.unhook_all()

    # Create a Rhythm from the pressed notes
    timings, keys = zip(*on_press.presses)

    rthm = Rhythm('Test', 'Jack', timings, keys, bpm, 0)
    seq = rthm.sequence

    # Print various aspects about the playback
    print(seq.downbeat_timings)
    print('Tuple timings', seq._tuple_timings)
    print('Triplet timings', seq._triplet_timings)
    [print(str(note)) for note in seq.notes]

    rthm.save_to_mongodb()
    print(f"Saved with ID {rthm._id}")
