"""Holds the JSONEncoder class"""

import json
from bson.objectid import ObjectId

class JSONEncoder(json.JSONEncoder):
    """Necessary to encode the ObjectId type with its string form"""

    def default(self, obj):
        if isinstance(obj, ObjectId):
            return str(obj)
        return json.JSONEncoder.default(self, obj)
