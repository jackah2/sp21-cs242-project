"""
Responsible for connecting to the MongoDB and creating the collection that will
be used to store rhythms.
"""

import os
import sys
import pymongo
from dotenv import load_dotenv

DOTENV = '.env'
HOST = 'MONGO_HOST'
PORT = 'MONGO_PORT'

COLLECTION = None

def _setup():
    """
    Sets up this script for use with MongoDB. Extracts values from the .env file
    and verifies they're filled in. Exits the program if .env does not exist.
    """
    if not os.path.exists(DOTENV):
        sys.exit(f"ERROR: Could not find '{DOTENV}'!")

    load_dotenv()
    host = os.getenv(HOST)
    port = os.getenv(PORT)

    if not host:
        sys.exit(f"ERROR: Missing '{HOST}' environment variable")
    if not port:
        sys.exit(F"ERROR: Missing '{PORT}' environment variable")

    # Get the client and rthm database
    client = pymongo.MongoClient(host, int(port))
    database = client.rthm

    # Get book/author collections
    global COLLECTION

    COLLECTION = database.rhythms

    # Add an index to allow searching by all text fields
    COLLECTION.create_index([('$**', 'text')])
_setup()
