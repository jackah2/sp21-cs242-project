"""Tests the /rthm/xml and /rthm/publish routes"""

import json

from rthm.models.rhythm import Rhythm


def get_json(tempo = None, timings = None, stickings = None):
    result = {}
    if tempo is not None:
        result['tempo'] = tempo
    if timings is not None:
        result['timings'] = timings
    if stickings is not None:
        result['stickings'] = stickings
    return result

def test_no_json(test_client):
    response = test_client.get('/rthm/xml')
    assert response.status_code == 415

def test_missing_header(test_client):
    response = test_client.get('/rthm/xml', data = json.dumps({}))
    assert response.status_code == 415

def test_no_tempo(test_client):
    response = test_client.get('/rthm/xml', json = get_json())
    assert response.status_code == 400

def test_non_integer_tempo(test_client):
    response = test_client.get('/rthm/xml', json = get_json('nan'))
    assert response.status_code == 400

def test_negative_tempo(test_client):
    response = test_client.get('/rthm/xml', json = get_json(-1))
    assert response.status_code == 400

def test_no_timings(test_client):
    response = test_client.get('/rthm/xml', json = get_json(0))
    assert response.status_code == 400

def test_non_integer_timing(test_client):
    response = test_client.get('/rthm/xml', json = get_json(0, ['nan']))
    assert response.status_code == 400

def test_timings_stickings_differ_in_length(test_client):
    response = test_client.get('/rthm/xml', json = get_json(0, [0], ['R', 'L']))
    assert response.status_code == 400

def test_xml_get(test_client, new_rhythm_json):
    response = test_client.get(f"/rthm/xml", json = new_rhythm_json)
    assert response.status_code == 200
    assert response.mimetype == 'text/xml'

def test_publish_put(test_client, new_rhythm_json):
    response = test_client.put(f"/rthm/publish", json = new_rhythm_json)
    assert response.status_code == 200
    _id = response.get_json().get('_id')
    rhythm = Rhythm.get_by_id_str(_id)
    assert rhythm is not None
    Rhythm.delete_by_id_str(_id)
