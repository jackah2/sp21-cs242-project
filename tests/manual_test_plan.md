# Manual Test Plan
This is the manual test plan for testing the frontend. Before starting, make sure MongoDB, the API, and the frontend are all running.

## Home screen
The home screen should look like the following

![Home screen](./pictures/home_screen.png)

## 404
If I go to `/rthm/no_such_page`, I should get a 404

![404](./pictures/404.png)

## Explore page
I should see all created Rthms when I go to the explore page.

![Explore page](./pictures/explore_page.png)

### Search By Query
If I search "Jack", the Rthms made by me should show up

![Results with "Jack"](./pictures/search_by_query.png)

### Search With No results
When there are no search results, an alert telling you so should pop up.

![No search results](./pictures/no_search_results.png)

## Details page
You should be able to see all data about the Rthm from the details page.

![Details page](./pictures/details_page.png)

### Invalid ID form
If I provide a malformed ID, I should be told it is invalid.

![Invalid ID](./pictures/invalid_id.png)

### Unknown Rthm
IF I provide an ID with no matching Rthm, I should be told there was Rthm found.

![Unknown Rthm](./pictures/unknown_rthm.png)

### Updating attribute
I will change the the title to "manual testing". After clicking the "Update Rthm" button, the page should refresh and the music should show the appropriate changes.

![Before update](./pictures/update_attribute_before.png)

![After update](./pictures/update_attribute_after.png)

### Invalid tempo
If I give a value without a number and update the Rthm, it will be discarded and the old value will be kept.

![Before invalid tempo](./pictures/invalid_tempo_before.png)

![After invalid tempo](./pictures/invalid_tempo_after.png)

### Download music
Clicking the "Download" button should start a download for a file called `<title>.musicxml`. You should be able to open this file in music editing software like MuseScore.

![Download music](./pictures/download_music.png)

![Opened in MuseScore](./pictures/opened_in_musescore.png)

### Deleting music
You should be redirected to the explore page and the Rthm should be gone.

![Before deleting](./pictures/delete_before.png)

![After deleting](./pictures/delete_after.png)

## Create
The creation screen should allow you to edit the title and author of the Rthm.

![Editable attributes](./pictures/create_editable_attributes.png)

### Metronome
I should be able to move the metronome slider to adjust the tempo. It should update the text accordingly.

![Changed metronome](./pictures/change_metronome.png)

### Record button
Clicking the record button should start the metronome and record my inputs.

![During recording](./pictures/recording_during.png)

![After recording](./pictures/recording_after.png)

### Publish button
The publish button should be disabled until something has been recorded. After that, clicking publish should bring you to the details page of the Rthm you just created.

![Published Rthm](./pictures/published_rthm.png)
