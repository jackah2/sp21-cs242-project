"""Tests the /rthm/search route"""

import rthm.utils.mongo as mongo


def test_search_all(test_client):
    response = test_client.get('/rthm/search')
    print(response)
    assert response.status_code == 200
    assert len(response.get_json().get('results')) == mongo.COLLECTION.count_documents({})

def test_search_empty_query(test_client):
    response = test_client.get('/rthm/search?query=')
    print(response)
    assert response.status_code == 200
    assert len(response.get_json().get('results')) == mongo.COLLECTION.count_documents({})

def test_search_dummy_title(test_client):
    response = test_client.get(f"/rthm/search?query=test")
    assert response.status_code == 200
    assert len(response.get_json().get('results')) >= 1

def test_search_dummy_author(test_client):
    response = test_client.get(f"/rthm/search?query=dummy")
    assert response.status_code == 200
    assert len(response.get_json().get('results')) >= 1
