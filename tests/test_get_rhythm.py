"""Tests the /rthm/<id> route"""

from bson.objectid import ObjectId

from rthm.models.rhythm import Rhythm


def test_invalid_id(test_client):
    response = test_client.get('/rthm/invalid_id')
    assert response.status_code == 400

def test_rhythm_not_found(test_client):
    unknown_id = str(ObjectId())
    response = test_client.get(f"/rthm/{unknown_id}")
    assert response.status_code == 404

def test_get(test_client, dummy_id):
    response = test_client.get(f"/rthm/{dummy_id}")
    assert response.status_code == 200
    data = response.get_json()
    assert data.get('title') == 'Test'
    assert data.get('author') == 'Dummy'
    assert data.get('tempo') == 60

def test_get_xml(test_client, dummy_id):
    response = test_client.get(f"/rthm/xml/{dummy_id}")
    assert response.status_code == 200
    assert response.mimetype == 'text/xml'

def test_delete(test_client, dummy_id):
    response = test_client.delete(f"/rthm/{dummy_id}")
    assert response.status_code == 200
    assert Rhythm.get_by_id_str(dummy_id) == None

def test_post_no_json(test_client, dummy_id):
    response = test_client.post(f"/rthm/{dummy_id}")
    assert response.status_code == 415

def test_post_unknown_attribute(test_client, dummy_id):
    response = test_client.post(f"/rthm/{dummy_id}", json = {'unknown': 'value'})
    assert response.status_code == 400

def test_post(test_client, dummy_id):
    response = test_client.post(f"/rthm/{dummy_id}", json = {'title': 'AWESOME'})
    assert response.status_code == 200
    rhythm = Rhythm.get_by_id_str(dummy_id)
    assert rhythm.title == 'AWESOME'
