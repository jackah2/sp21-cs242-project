"""Create fixtures for pytest test cases"""

import pytest

from rthm.api import create_app
from rthm.models.rhythm import Rhythm

@pytest.fixture(scope='module')
def test_client():
    app = create_app()
    with app.test_client() as client:
        with app.app_context():
            yield client

@pytest.fixture(scope='module')
def dummy_rhythm():
    return Rhythm(
        title='Test',
        author='Dummy',
        timings=[0, 1, 2, 3, 4],
        stickings=['R', 'L', 'R', 'L', 'R'],
        tempo=60,
        time_created=0
    )

@pytest.fixture
def dummy_id(dummy_rhythm):
    return str(dummy_rhythm._id)

@pytest.fixture
def new_rhythm_json():
    return {
        'title': 'New title',
        'author': 'New author',
        'timings': [0],
        'stickings': ['R'],
        'tempo': 60,
    }

@pytest.fixture(autouse=True)
def create_dummy_rhythm(dummy_rhythm: Rhythm):
    dummy_rhythm.save_to_mongodb()
    yield
    Rhythm.delete_by_id(dummy_rhythm._id)
