import React from 'react';
import { Route, Switch, BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'use-http';

import './App.css';
import Header from './Components/Header';
import Create from './Pages/Create';
import Explore from './Pages/Explore';
import Details from './Pages/Details';
import Home from './Pages/Home';
import NoSuchPage from './Pages/NoSuchPage';

function App() {
  return (
    <Provider>
      <Router basename="/rthm">
        <div className="App-container">
          <Header/>
          <div className="App px-4 py-2">
            <Switch>
              <Route path="/create" component={Create} />
              <Route path="/explore/:rthm_id" component={Details} />
              <Route path="/explore" component={Explore} />
              <Route path="/" exact component={Home} />
              <Route path="*" component={NoSuchPage}/>
            </Switch>
          </div>
        </div>
      </Router>
    </Provider>
  );
}

export default App;
