const NoSuchPage = () => (
  <div className="Centered">
    404 | Page not found
  </div>
);

export default NoSuchPage;
