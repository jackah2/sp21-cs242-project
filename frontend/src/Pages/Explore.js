import React, { useState } from "react";
import useFetch from "use-http";
import Alert from "react-bootstrap/Alert";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import FormControl from "react-bootstrap/FormControl";

import Loading from "../Components/Loading";
import RthmList from "../Components/RthmList";

const Explore = () => {
  const [ query, setQuery ] = useState('');
  const [ searchContents, setSearchContents ] =  useState('');
  const { loading, error, data } = useFetch(`/rthm/search?query=${query}`, [query]);

  const innerRender = () => {
    if (loading) return <Loading />;

    if (error) return <Alert variant="danger">{data.message || "Server error"}</Alert>;

    return <RthmList rthms={data.results} />;
  }

  return (
    <div>
      <Form inline className="container-flow pt-2 row w-100 m-0">
        <FormControl
          type="text"
          placeholder="Search"
          className="col mr-1"
          onChange={event => setSearchContents(event.target.value)}
        />
        <Button
          type="submit"
          variant="primary"
          onClick={event => {
            event.preventDefault();
            setQuery(searchContents);
          }}
        >
          Search
        </Button>
      </Form>
      <hr />
      {innerRender()}
    </div>
  )
};

export default Explore;
