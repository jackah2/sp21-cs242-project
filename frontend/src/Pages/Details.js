import { useParams } from "react-router";
import useFetch from "use-http";
import Alert from "react-bootstrap/Alert";

import Loading from "../Components/Loading";
import Rthm from "../Components/Rthm";

const Details = () => {
  const { rthm_id } = useParams();
  const { loading, error, data } = useFetch(`/rthm/${rthm_id}`, [rthm_id])

  if (loading) return <Loading />

  if (error) return <Alert variant="danger">{data.message}</Alert>

  return <Rthm rthm={data} />
};

export default Details;
