import React from "react";
import Jumbotron from "react-bootstrap/Jumbotron";

const Home = () => (
  <Jumbotron>
    <h1>Rthm</h1>
    <p>This web-app was created by <strong>Jack Henhapl</strong>.</p>
  </Jumbotron>
);

export default Home;
