import { useEffect, useRef, useState } from "react";
import { useHistory } from "react-router-dom";
import Card from "react-bootstrap/Card";
import Alert from "react-bootstrap/Alert";
import Button from "react-bootstrap/Button";
import useFetch from "use-http";

import Metronome from "../Components/Metronome";
import RthmField from "../Components/RthmField";

const STICKINGS = {
  ShiftRight: "R",
  ShiftLeft: "L",
}

const Create = () => {
  const { put } = useFetch("/rthm/publish")
  const { push } = useHistory();

  const first = useRef(true);
  const recordingRef = useRef(false);
  const [ recording, setRecording ] = useState(false);
  const [ title, setTitle ] = useState("Awesome title");
  const [ author, setAuthor ] = useState("You!");
  const [ tempo, setTempo ] = useState(120);
  // const [ notes ] = useState(emptyNotes);
  const [ timings, setTimings ] = useState([]);
  const [ stickings, setStickings ] = useState([]);

  useEffect(() => {
    // Make sure this listener is only registered once
    if (!first.current) return;
    first.current = false

    document.addEventListener("keydown", event => {
      // If we're not recording, don't add the notes
      if (!recordingRef.current) return;

      const sticking = STICKINGS[event.code];
      const time = event.timeStamp / 1000

      if (!sticking) return;

      setTimings(prevTimings => [...prevTimings, time])
      setStickings(prevStickings => [...prevStickings, sticking])
    });
  }, [timings, stickings]);

  const updateRecording = (newVal) => {
    recordingRef.current = newVal;
    setRecording(newVal);
  }

  const clearNotes = () => {
    setTimings([]);
    setStickings([]);
  }

  const handlePublish = async () => {
    const data = await put({
      title,
      author,
      tempo,
      stickings,
      timings,
    });
    push(`/explore/${data._id}`);
  };

  return (
    <>
      <Card>
        <Card.Body>
          <RthmField
            value={title}
            placeholder="Title"
            onChange={event => setTitle(event.target.value)}
          />
          <RthmField
            value={author}
            placeholder="Author"
            onChange={event => setAuthor(event.target.value)}
          />
        </Card.Body>
        <Metronome
          startingBpm={tempo}
          onBpmChange={(bpm) => {
            setTempo(bpm);
            clearNotes();
          }}
          onToggleRecording={record => {
            updateRecording(record);
            if (record) {
              clearNotes();
            }
          }}
        />
      </Card>
      <Alert variant="secondary" className="my-2">
        <p>
          Click <strong>Record</strong> to begin recording. <strong>Right Shift</strong> will
          play a <strong>right hand</strong> note and <strong>Left Shift</strong> will play
          a <strong>left hand</strong> note.
        </p>
      </Alert>
      <Button
        onClick={handlePublish}
        disabled={recording || stickings.length === 0}
      >
        Publish
      </Button>
    </>
  )
};

export default Create;
