import Form from "react-bootstrap/Form";
import InputGroup from 'react-bootstrap/InputGroup';

const RthmField = ({ readOnly=false, value, placeholder, onChange }) => (
  <InputGroup className="mb-0" onChange={onChange}>
    <InputGroup.Prepend>
      <InputGroup.Text id="basic-addon1">{placeholder}</InputGroup.Text>
    </InputGroup.Prepend>
    <Form.Control
      readOnly={readOnly}
      placeholder={placeholder}
      defaultValue={value}
      aria-describedby="basic-addon1"
    />
  </InputGroup>
);

export default RthmField
