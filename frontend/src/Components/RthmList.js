import { useHistory } from "react-router";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Alert from "react-bootstrap/esm/Alert";

const RthmPreview = ({ rthm: { _id, title, author, tempo, time_created } }) => {
  const { push } = useHistory();
  const formattedTime = new Intl.DateTimeFormat("en", {
    month: "long",
    day: "numeric",
    year: "numeric"
  }).format(time_created * 1000);

  return (
    <Card className="shadow-sm my-3">
      <div className="p-2">
        <Card.Title>{title}</Card.Title>
        <Card.Subtitle className="mb-2 text-muted">{author}</Card.Subtitle>
      </div>
      <Card.Footer>
        <strong>{tempo} BPM</strong> | {formattedTime}
        <Button
          size="sm"
          className="float-right"
          variant="outline-secondary"
          onClick={() => push(`/explore/${_id}`)}
        >
          Details
        </Button>
      </Card.Footer>
    </Card>
  )
};

const RthmList = ({ rthms }) => {
  var items = rthms.map((rthm) => (
    <RthmPreview
      key={rthm._id}
      rthm={rthm}
    />
  ))
  console.log(items)
  if (items.length === 0) {
    items = <Alert variant="secondary">No Rthms found!</Alert>
  }
  return <>{items}</>
};

export default RthmList;
