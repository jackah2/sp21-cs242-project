import Card from "react-bootstrap/Card";
import Form from "react-bootstrap/Form";

import OpenSheetMusicDisplay from "./OpenSheetMusicDisplay";
import DownloadButton from "./DownloadButton";

const RthmScore = ({ rthm: { _id, title, time_created } }) => {
  const formattedTime = new Intl.DateTimeFormat("en", {
    month: "long",
    day: "numeric",
    year: "numeric"
  }).format(time_created * 1000);

  const xmlUrl = `/rthm/xml/${_id}`

  return (
    <Card>
      <Card.Body>
        <OpenSheetMusicDisplay
          file={xmlUrl}
        />
      </Card.Body>
      <Card.Footer>
        <Form.Row>
          <strong>{formattedTime}</strong>
          <div className="ml-auto">
            <DownloadButton url={xmlUrl} fileName={title} />
          </div>
        </Form.Row>
      </Card.Footer>
    </Card>
  )
};

export default RthmScore;
