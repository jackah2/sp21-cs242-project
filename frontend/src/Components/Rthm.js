import React, { useState } from "react";
import useFetch from "use-http";
import { useHistory } from "react-router-dom";

import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import Form from "react-bootstrap/Form";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";

import RthmScore from "./RthmScore";
import RthmField from "./RthmField";

const Rthm = ({ rthm }) => {
  const { post, del } = useFetch(`/rthm/${rthm._id}`);
  const { push } = useHistory();

  const [ title, setTitle ] = useState(rthm.title);
  const [ author, setAuthor ] = useState(rthm.author);
  const [ tempo, setTempo ] = useState(rthm.tempo);
  const [ editing, setEditing ] = useState(false);

  const handleUpdate = async () => {
    const data = { title, author, tempo};
    setEditing(false);
    await post(data);
    window.location.reload();
  };

  const handleDelete = async () => {
    await del();
    push("/explore");
    window.location.reload();
  };

  return (
    <>
      <Card>
        <Card.Body>
          <Form>
              <RthmField
                readOnly={!editing}
                value={title}
                placeholder="Title"
                onChange={event => setTitle(event.target.value)}
              />
              <RthmField
                readOnly={!editing}
                value={author}
                placeholder="Author"
                onChange={event => setAuthor(event.target.value)}
              />
              <RthmField
                readOnly={!editing}
                value={tempo}
                placeholder="Tempo"
                onChange={event => setTempo(parseInt(event.target.value) || rthm.tempo)}
              />
            </Form>
        </Card.Body>
        <Card.Footer>
          <Form.Row>
            <Form.Switch
              id="toggle-editing"
              label="Toggle editing"
              checked={editing}
              onChange={() => setEditing(!editing)}
            />
            <Button
              size="sm"
              variant="primary"
              className="ml-auto mr-0"
              disabled={!editing}
              onClick={() => handleUpdate()}
            >
              Update Rthm
            </Button>
            <OverlayTrigger
              key="delete"
              placement="top"
              overlay={<Tooltip>Double click to delete</Tooltip>}
            >
              <Button
                size="sm"
                variant="danger"
                className="ml-1"
                disabled={!editing}
                onDoubleClick={() => handleDelete()}
              >
                Delete Rthm
              </Button>
            </OverlayTrigger>
          </Form.Row>
        </Card.Footer>
      </Card>
      <hr />
      <RthmScore rthm={rthm}/>
    </>
  )
};

export default Rthm;
