import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Link } from 'react-router-dom';

const Header = () => (
  <Navbar sticky="top" bg="dark" variant="dark" expand="lg">
    <Navbar.Brand as={Link} to="/">Rthm</Navbar.Brand>
    <Nav className="mr-auto">
      <Nav.Link as={Link} to="/create">Create</Nav.Link>
      <Nav.Link as={Link} to="/explore">Explore</Nav.Link>
    </Nav>
  </Navbar>
);

export default Header;
