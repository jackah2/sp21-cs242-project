import { useState } from 'react'
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import FormControl from "react-bootstrap/FormControl";

import clickSound from '../Assets/click.wav'

const CLICK = new Audio(clickSound);

/**
 * This is a modified version of
 *  https://codesandbox.io/s/9oy853m8xy?file=/src/Components/Metronome.js
 */
const Metronome = ({ startingBpm=120, onBpmChange, onToggleRecording }) => {
  const [ recording, setRecording ] = useState(false);
  const [ bpm, setBpm ] = useState(startingBpm);
  const [ timer, setTimer ] = useState();

  const updateTimer = (newBpm) => setTimer(setInterval(playClick, (60 / newBpm) * 1000));

  const playClick = () => {
    CLICK.play();
  }

  const handleBpmChange = event => {
    const newBpm = event.target.value
    if (recording) {
      clearInterval(timer);
      updateTimer(newBpm);
    }

    onBpmChange(newBpm);
    setBpm(newBpm);
  }

  const handleStartStop = () => {
    if (recording) {
      clearInterval(timer);
    } else {
      updateTimer(bpm);
      playClick();
    }

    onToggleRecording(!recording);
    setRecording(!recording);
  }

  return (

    <Card>
      <Card.Body>
        <Card.Title className="Centered">{bpm} BPM</Card.Title>
        <FormControl type="range" value={bpm} min="30" max="240" onChange={handleBpmChange}/>
      </Card.Body>
      <Button onClick={handleStartStop} variant={recording ? "warning" : "danger"}>
        {recording ? 'Stop' : 'Record'}
      </Button>
    </Card>
  )
};

export default Metronome;
