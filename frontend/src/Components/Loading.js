import Spinner from "react-bootstrap/Spinner";

const Loading = () => (
  <div className="Centered">
    <Spinner animation="border" role="status">
      <span className="sr-only">Loading...</span>
    </Spinner>
  </div>
);

export default Loading;
