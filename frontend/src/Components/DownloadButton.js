import Button from "react-bootstrap/Button";

// This is used to fetch a Rthm's XML and download it when clicked
const DownloadButton = ({ url, fileName }) => {
  const downloadFile = () => {
    fetch(url)
      .then(response => {
        response.blob().then(blob => {
          const objUrl = window.URL.createObjectURL(blob);
          let a = document.createElement("a");
          a.href = objUrl;
          a.download = `${fileName}.musicxml`;
          a.click();
        });
      });
  };

  return (
    <Button
      size="sm"
      variant="primary"
      onClick={downloadFile}
    >
      Download
    </Button>
  )
}

export default DownloadButton;
