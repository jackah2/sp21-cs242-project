# sp21-cs242-project

# Installation

## Config
```bash
cp .env-empty .env
```
Populate `.env` with the appropriate values

## Dependencies
* Numpy
* Keyboard
* scamp
* pymongo

```bash
python -m venv venv
source ./venv/bin/activate
pip install -r requirements.txt
```

# Running

## Listener
The listener can be used to manually enter in rhythms with the keyboard.

```bash
python -m rthm.listener <bpm>
```

## API
The API can be started with a given port with `--port <port>` or in debug mode with `--debug`.

```bash
python -m rthm.api
```

## Testing
The API can be tested with the following:

```bash
pytest
```

The manual test plan is located [here](./tests/manual_test_plan.md)
